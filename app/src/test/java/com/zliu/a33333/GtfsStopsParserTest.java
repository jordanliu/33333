package com.zliu.a33333;

import com.zliu.a33333.model.Stop;
import com.zliu.a33333.model.StopManager;
import com.zliu.a33333.model.exception.StopException;
import com.zliu.a33333.parser.GtfsStopsParser;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GtfsStopsParserTest {

    public double EPSILON = 0.00001;

    @Before
    public void runBefore() throws FileNotFoundException, StopException {
        FileReader testFileReader = new FileReader("./src/main/res/raw/stops_txt");
        Scanner sc = new Scanner(testFileReader);
        GtfsStopsParser.parseStops(sc);
    }

    @Test
    public void testFirstItem() {
        Stop firstStop = StopManager.getInstance().getStopWithNumber(56030);

        assertEquals("WHITE ROCK CENTRE BAY 2", firstStop.getName());
        assertTrue(epsEquals(49.031009, firstStop.getLocn().latitude));
        assertTrue(epsEquals(-122.801843, firstStop.getLocn().longitude));
    }

    private boolean epsEquals(double d1, double d2) {
        return (Math.abs(d1-d2) <= EPSILON);
    }
}