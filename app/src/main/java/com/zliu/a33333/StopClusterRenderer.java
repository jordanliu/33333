package com.zliu.a33333;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.ClusterRenderer;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.zliu.a33333.model.Stop;

/**
 * Created by jordan on 16/10/17.
 */

public class StopClusterRenderer extends DefaultClusterRenderer<Stop> {

    Context mContext;
    Bitmap scaledIcon;

    public StopClusterRenderer(Context context, GoogleMap map, ClusterManager clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_bus_stop);
        scaledIcon = Bitmap.createScaledBitmap(icon, (int) (icon.getWidth() * 0.6), (int) (icon.getHeight() * 0.6), true);
    }

    @Override
    protected void onBeforeClusterItemRendered(Stop item, MarkerOptions options) {
        options.icon(BitmapDescriptorFactory.fromBitmap(scaledIcon));
        super.onBeforeClusterItemRendered(item, options);
    }
}
