package com.zliu.a33333.model;



import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.*;

/**
 * Represents a bus stop with an number, name, location (lat/lon)
 * set of routes which stop at this stop and a list of arrivals.
 */
public class Stop implements Iterable<Arrival>, ClusterItem {
    private Set<Route> routes = new HashSet<>();
    private int number;
    private String name;
    private LatLng locn;
    private List<Arrival> arrivals;

    /**
     * Constructs a stop with given number, name and location.
     * Set of routes and list of arrivals are empty.
     *
     * @param number    the number of this stop
     * @param name      name of this stop
     * @param locn      location of this stop
     */
    public Stop(int number, String name, LatLng locn) {
        arrivals = new ArrayList<>();
        this.number = number;
        this.name = name;
        this.locn = locn;
    }

    /**
     * getter for name
     * @return      the name
     */
    public String getName() {
        return name;
    }

    /**
     * getter for locn
     * @return      the location
     */
    public LatLng getLocn() {
        return locn;
    }

    /**
     * getter for number
     * @return      the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * getter for set of routes
     * @return      an unmodifiable set of routes using this stop
     */
    public Set<Route> getRoutes() {
        return Collections.unmodifiableSet(routes);
    }

    /**
     * Add route to set of routes with stops_txt at this stop.
     *
     * @param route  the route to add
     */
    public void addRoute(Route route) {
        if (!onRoute(route)) {
            routes.add(route);
            route.addStop(this);
        }
    }

    public List<Arrival> getArrivals() {
        return Collections.unmodifiableList(this.arrivals);
    }

    /**
     * Remove route from set of routes with stops_txt at this stop
     *
     * @param route the route to remove
     */
    public void removeRoute(Route route) {
        if (onRoute(route)) {
            routes.remove(route);
            route.removeStop(this);
        }
    }

    /**
     * Determine if this stop is on a given route
     * @param route  the route
     * @return  true if this stop is on given route
     */
    public boolean onRoute(Route route) {
        return routes.contains(route);
    }

    /**
     * Add bus arrival travelling on a particular route at this stop.
     * Arrivals are to be sorted in order by arrival time
     *
     * @param arrival  the bus arrival to add to stop
     */
    public void addArrival(Arrival arrival) {
        arrivals.add(arrival);
        Collections.sort(arrivals);
    }

    /**
     * Remove all arrivals from this stop
     */
    public void clearArrivals() {
        arrivals.clear();
    }

    /**
     * Two stops_txt are equal if their numbers are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stop that = (Stop) o;

        return number == that.number;

    }

    /**
     * Two stops_txt are equal if their numbers are equal.
     * Therefore hashCode only pays attention to number.
     */
    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public Iterator<Arrival> iterator() {
        // Do not modify the implementation of this method!
        return arrivals.iterator();
    }

    /**
     * setter for name
     *
     * @param name      the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * setter for location
     * @param locn      the new location
     */
    public void setLocn(LatLng locn) {
        this.locn = locn;
    }

    @Override
    public LatLng getPosition() {
        return this.locn;
    }

    @Override
    public String getTitle() {
        return this.name;
    }

    @Override
    public String getSnippet() {
        // TODO: do some shit with this
        return "";
    }
}
