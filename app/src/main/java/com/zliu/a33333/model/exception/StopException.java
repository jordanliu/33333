package com.zliu.a33333.model.exception;

/**
 * Represents exception raised when errors occur with Stops
 */
public class StopException extends Exception {
    public StopException(String msg) {
        super(msg);
    }
}

