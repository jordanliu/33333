package com.zliu.a33333.parser;

/**
 * Created by jordan on 08/10/17.
 */

public class ArrivalsDataMissingException extends Exception {
    public ArrivalsDataMissingException(String s) {
        super(s);
    }
}
