package com.zliu.a33333.parser;

import android.support.v4.content.res.TypedArrayUtils;

import com.google.android.gms.maps.model.LatLng;
import com.zliu.a33333.model.Stop;
import com.zliu.a33333.model.StopManager;
import com.zliu.a33333.model.exception.StopException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by jordan on 26/09/17.
 */

public class GtfsStopsParser {

    // pretty nice cuz we can pass a scanner from any type of InputStream
    public static void parseStops(Scanner sc) throws StopException {
        // discard the first line
        sc.nextLine();

        while (sc.hasNextLine()) parseOneStop(sc.nextLine());
    }

    // this method is slow as fuck, do not use
    public static void parseStops(String stopData) throws StopException {
        List<String> stopDataLines = new LinkedList<>(Arrays.asList(stopData.split("\\r?\\n")));

        // the first line of gtfs data is always some bullshit
        stopDataLines.remove(0);

        for (String line : stopDataLines) parseOneStop(line);
    }

    private static void parseOneStop(String line) throws StopException {
        try {
            String[] lineParts = line.split(",");

            // this is pretty much just to deal with the last line having
            // a hanging newline instead of being completely empty
            if (lineParts.length < 3 || lineParts[1].equals(" ")) return;

            int number = Integer.parseInt(lineParts[1]);
            String name = lineParts[2];
            double lat = Double.parseDouble(lineParts[4]);
            double lon = Double.parseDouble(lineParts[5]);

            LatLng posn = new LatLng(lat, lon);


            synchronized (StopManager.getInstance()){
                StopManager.getInstance().getStopWithNumber(number, name, posn);
            }
        }
        // this should never happen if translink doesnt give us shit data
        catch (NumberFormatException e) {
            throw new StopException("could not parse stops_txt" + e.getMessage());
        }
    }
}
