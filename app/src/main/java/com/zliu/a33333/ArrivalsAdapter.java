package com.zliu.a33333;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zliu.a33333.model.Arrival;
import com.zliu.a33333.model.Stop;

/**
 * Created by jordan on 17/10/17.
 */

public class ArrivalsAdapter extends RecyclerView.Adapter<ArrivalsAdapter.ArrivalViewHolder> {

    private Stop stop;

    public class ArrivalViewHolder extends RecyclerView.ViewHolder {
        public TextView routeNumber, timeToArrival, routeName;

        public ArrivalViewHolder(View view) {
            super(view);
            routeNumber = (TextView) view.findViewById(R.id.routeNumber);
            timeToArrival = (TextView) view.findViewById(R.id.timeToArrival);
            routeName = (TextView) view.findViewById(R.id.routeName);
        }
    }

    public ArrivalsAdapter(Stop stop) {
        this.stop = stop;
    }

    @Override
    public ArrivalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.arrival_layout, parent, false);
        return new ArrivalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ArrivalViewHolder holder, int position) {
        Arrival arr = stop.getArrivals().get(position);
        holder.routeName.setText(arr.getDestination());
        String tta = arr.getTimeToStopInMins() + " mins";
        holder.timeToArrival.setText(tta);
        holder.routeNumber.setText(arr.getRoute().getNumber());
    }

    @Override
    public int getItemCount() {
        return stop.getArrivals().size();
    }
}
