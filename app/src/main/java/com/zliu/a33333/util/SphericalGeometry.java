package com.zliu.a33333.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Spherical Geometry Utilities
 */
public class SphericalGeometry {
    private static final int RADIUS = 6371000;   // radius of earth in metres

    /**
     * Find distance in metres between two lat/lon points
     *
     * @param p1  first point
     * @param p2  second point
     * @return distance between p1 and p2 in metres
     */
    public static double distanceBetween(LatLng p1, LatLng p2) {
        double lat1 = p1.latitude / 180.0 * Math.PI;
        double lat2 = p2.latitude / 180.0 * Math.PI;
        double deltaLon = (p2.longitude - p1.longitude) / 180.0 * Math.PI;
        double deltaLat = (p2.latitude - p1.latitude) / 180.0 * Math.PI;

        double a = Math.sin(deltaLat / 2.0) * Math.sin(deltaLat / 2.0)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.sin(deltaLon / 2.0) * Math.sin(deltaLon / 2.0);
        double c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return c * RADIUS;
    }
}
