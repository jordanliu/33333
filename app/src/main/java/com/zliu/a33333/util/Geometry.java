package com.zliu.a33333.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Compute relationships between points, lines, and rectangles represented by LatLng objects
 */
public class Geometry {
    /**
     * Return true if the point is inside of, or on the boundary of, the rectangle formed by northWest and southeast
     * @param northWest         the coordinate of the north west corner of the rectangle
     * @param southEast         the coordinate of the south east corner of the rectangle
     * @param point             the point in question
     * @return                  true if the point is on the boundary or inside the rectangle
     */
    public static boolean rectangleContainsPoint(LatLng northWest, LatLng southEast, LatLng point) {
        double nwlat, nwlon, selat, selon, lat, lon;
        nwlat = northWest.latitude;
        nwlon = northWest.longitude;
        selat = southEast.latitude;
        selon = southEast.longitude;
        lat = point.latitude;
        lon = point.longitude;
        return between(selat, nwlat, lat) && between(nwlon, selon, lon);
    }

    /**
     * Return true if the rectangle intersects the line
     *
     * This version isn't perfect, it merely computes whether the bounding rectangle
     * of the line intersects the rectangle. For our purposes, this is close enough.
     *
     * @param northWest         the coordinate of the north west corner of the rectangle
     * @param southEast         the coordinate of the south east corner of the rectangle
     * @param src               one end of the line in question
     * @param dst               the other end of the line in question
     * @return                  true if any point on the line is on the boundary or inside the rectangle
     */
    public static boolean rectangleIntersectsLine(LatLng northWest, LatLng southEast, LatLng src, LatLng dst) {
        double nwlat, nwlon, selat, selon, nwlat2, nwlon2, selat2, selon2;
        nwlat = northWest.latitude;
        nwlon = northWest.longitude;
        selat = southEast.latitude;
        selon = southEast.longitude;

        if (src.latitude < dst.latitude) {
            nwlat2 = dst.latitude;
            selat2 = src.latitude;
        } else {
            nwlat2 = src.latitude;
            selat2 = dst.latitude;
        }
        if (src.longitude > dst.longitude) {
            nwlon2 = dst.longitude;
            selon2 = src.longitude;
        } else {
            nwlon2 = src.longitude;
            selon2 = dst.longitude;
        }
        boolean ans = !(selon < nwlon2 || selon2 < nwlon || nwlat < selat2 || nwlat2 < selat);
        return ans;
    }

    /**
     * A utility method that you might find helpful in implementing the two previous methods
     * Return true if x is >= lwb and <= upb
     * @param lwb      the lower boundary
     * @param upb      the upper boundary
     * @param x         the value in question
     * @return          true if x is >= lwb and <= upb
     */
    private static boolean between(double lwb, double upb, double x) {
        return lwb <= x && x <= upb;
    }
}
