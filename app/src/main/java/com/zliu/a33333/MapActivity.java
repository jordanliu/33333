package com.zliu.a33333;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.clustering.ClusterManager;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.zliu.a33333.model.Arrival;
import com.zliu.a33333.model.Stop;
import com.zliu.a33333.model.StopManager;
import com.zliu.a33333.model.exception.StopException;
import com.zliu.a33333.parser.ArrivalsDataMissingException;
import com.zliu.a33333.parser.ArrivalsParser;
import com.zliu.a33333.parser.GtfsStopsParser;
import com.zliu.a33333.provider.HttpArrivalDataProvider;
import com.zliu.a33333.util.SphericalGeometry;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static android.location.LocationProvider.OUT_OF_SERVICE;
import static android.location.LocationProvider.TEMPORARILY_UNAVAILABLE;

public class MapActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, ClusterManager.OnClusterItemClickListener<Stop>, GoogleApiClient.ConnectionCallbacks,
        LocationListener, GoogleApiClient.OnConnectionFailedListener, OnSuccessListener<Location> {

    private GoogleMap mMap;

    private static String LOG_TAG = "MapActivity";

    private ClusterManager<Stop> stopClusterManager;

    LinearLayout stopInfoView;

    GoogleApiClient mGoogleApiClient;

    LatLng myLocation;

    LatLng viewportCenter;

    SlidingUpPanelLayout panelLayout;



    //TODO: use this to add stops to clusterer that are within 10 km or smth
    private LatLng myLocn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        stopInfoView = (LinearLayout) findViewById(R.id.panel_linear_layout);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.arrivals_recyclerview);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(null);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        panelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        panelLayout.setTouchEnabled(false);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in UBC and move the camera
        LatLng ubcMarker = new LatLng(49.2628755, -123.2473369);
        //mMap.addMarker(new MarkerOptions().position(ubcMarker).title("Marker in UBC"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubcMarker, 19.0F));
        mMap.setOnCameraIdleListener(this);

        Thread stopLoadThread = new Thread(this::loadStops);

        stopLoadThread.run();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubcMarker, 16.0F));

        stopClusterManager = new ClusterManager<Stop>(this, mMap);
        StopClusterRenderer renderer = new StopClusterRenderer(this, mMap, stopClusterManager);
        stopClusterManager.setRenderer(renderer);
        mMap.setOnMarkerClickListener(stopClusterManager);
        stopClusterManager.setOnClusterItemClickListener(this);

        mMap.setOnMyLocationButtonClickListener(this);
        UiSettings settings = mMap.getUiSettings();
        settings.setMyLocationButtonEnabled(true);


        viewportCenter = mMap.getCameraPosition().target;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] strings = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, strings, 20);
        } else {
            // we have location permission
            mMap.setMyLocationEnabled(true);
            Task<Location> l = LocationServices.getFusedLocationProviderClient(this).getLastLocation();
            l.addOnSuccessListener(this);
        }

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        new UpdateVisibleMarkersAsyncTask().execute(bounds);
    }

    private void loadStops() {
        InputStream is = getResources().openRawResource(R.raw.stops_txt);
        Scanner sc = new Scanner(is);
        try {
            GtfsStopsParser.parseStops(sc);
            sc.close();
        } catch (StopException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }


    @Override
    public boolean onClusterItemClick(Stop stop) {
        LoadArrivalsAsyncTask task = (LoadArrivalsAsyncTask) new LoadArrivalsAsyncTask().execute(stop);
        // Toast.makeText(this, "onClusterItemClick: " + stop.getName(), Toast.LENGTH_LONG).show();
        return true;
    }


    @Override
    public void onCameraIdle() {
        // draw visible markers when the camera stops moving
        // maybe consider threading this in the future

        // debugging shit
        Log.i(LOG_TAG, "onCameraIdle invoked");

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        viewportCenter = mMap.getCameraPosition().target;

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_bus_stop);

        /*
        Runnable runnable = () -> {
            synchronized (StopManager.getInstance()) {
                for (Stop s : StopManager.getInstance()) {
                    // i hope these are the only cases that require handling
                    if ((myLocation != null && SphericalGeometry.distanceBetween(myLocation, s.getLocn()) < 5000)
                        || SphericalGeometry.distanceBetween(viewportCenter, s.getLocn()) < 5000
                            || bounds.contains(s.getLocn())) {
                        stopClusterManager.addItem(s);
                    }
                    else {
                        stopClusterManager.removeItem(s);
                    }
                }
            }
        };


        new Thread(runnable).run();
        */

        new UpdateVisibleMarkersAsyncTask().execute(bounds);

        stopClusterManager.cluster();

        // i'm hoping i can get away with manually calling callbacks instead of
        // setting stopClusterManager as OnCameraIdleListener
        stopClusterManager.onCameraIdle();

    }

    @Override
    public boolean onMyLocationButtonClick() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Task<Location> locationTask = LocationServices.getFusedLocationProviderClient(this).getLastLocation();
            Location location = locationTask.getResult();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 18.0F));
            mMap.animateCamera(CameraUpdateFactory.zoomBy(1.0F));
            this.onCameraIdle();
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 20) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                Toast.makeText(this, "Could not get location permission", Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Location l = LocationServices.getFusedLocationProviderClient(this).getLastLocation().getResult();
            myLocation = new LatLng(l.getLatitude(), l.getLongitude());
        }
        else {
            // some default point
            myLocation = new LatLng(49.2628755, -123.2473369);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Google API connection suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        if (i == OUT_OF_SERVICE) {
            Toast.makeText(this, "Location provider out of service", Toast.LENGTH_SHORT).show();
        }
        else if (i == TEMPORARILY_UNAVAILABLE) {
            Toast.makeText(this, "Location provider temporarily unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProviderEnabled(String s) {
        // TODO: put something here
    }

    @Override
    public void onProviderDisabled(String s) {
        // TODO: put something here
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Could not connect to Google API", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Location location) {
        // this literally only gets called once when the map is ready
        Location loc = location;
        LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
    }


    private class LoadArrivalsAsyncTask extends AsyncTask<Stop, Integer, Stop> {

        LinearLayout layout;
        ProgressBar b;

        @Override
        protected void onPreExecute() {
            layout = stopInfoView;
            b = new ProgressBar(getApplicationContext());
            if (layout.findViewById(Integer.MAX_VALUE) != null) return;
            b.setId(Integer.MAX_VALUE);
            b.setIndeterminate(true);
            b.setMinimumHeight(80);
            b.setMinimumWidth(80);
            layout.addView(b, 0);
            panelLayout.setTouchEnabled(false);
        }

        @Override
        protected Stop doInBackground(Stop... stops) {
            try {
                HttpArrivalDataProvider provider = new HttpArrivalDataProvider(stops[0]);
                String jsonResponse = provider.dataSourceToString();
                stops[0].clearArrivals();
                ArrivalsParser.parseArrivals(stops[0], jsonResponse);
                panelLayout.setTouchEnabled(true);
            }
            catch (IOException e) {
                Log.e(LOG_TAG, "failed to download arrivals\n" + e.getMessage());
                e.printStackTrace();
                return null;
            } catch (ArrivalsDataMissingException e) {
                Log.e(LOG_TAG, "arrivals data is missing\n" + e.getMessage());
                e.printStackTrace();
                return null;
            }
            catch (JSONException e) {
                Log.e(LOG_TAG, "parsing arrivals JSON failed\n" + e.getMessage());
                e.printStackTrace();
                return null;
            }
            return stops[0];
        }

        @Override
        protected void onPostExecute(Stop stop) {

            TextView stopNumber = (TextView) findViewById(R.id.stop_name_textview);
            layout.removeView(b);
            if (stop == null) {
                stopNumber.setText(R.string.arrivals_fail);
                panelLayout.setTouchEnabled(false);
                RecyclerView view = (RecyclerView) findViewById(R.id.arrivals_recyclerview);
                view.setAdapter(null);
                view.invalidate();
                return;
            }

            stopNumber.setText(stop.getName());
            panelLayout.setTouchEnabled(true);
            RecyclerView view = (RecyclerView) findViewById(R.id.arrivals_recyclerview);
            ArrivalsAdapter adapter = new ArrivalsAdapter(stop);
            view.setAdapter(adapter);
            view.invalidate();
        }
    }

    private class UpdateVisibleMarkersAsyncTask extends AsyncTask<LatLngBounds, Void, Void> {

        @Override
        protected Void doInBackground(LatLngBounds... bounds) {
            Log.i(LOG_TAG, "UpdateVisibleMarkersAsyncTask ran");
            synchronized (StopManager.getInstance()) {
                for (Stop s : StopManager.getInstance()) {
                    // i hope these are the only cases that require handling
                    if ((myLocation != null && SphericalGeometry.distanceBetween(myLocation, s.getLocn()) < 5000)
                            || SphericalGeometry.distanceBetween(viewportCenter, s.getLocn()) < 5000
                            || bounds[0].contains(s.getLocn())) {
                        stopClusterManager.addItem(s);
                    }
                    else {
                        stopClusterManager.removeItem(s);
                    }
                }
            }
            return null;
        }
    }
}
